import { Game } from "./module/types/wlq/game";
import { Params } from "./module/types/wlq/params";
import { Queue } from "./module/types/wlq/queue";
export { Game, Params, Queue };
declare const _default;
export default _default;
