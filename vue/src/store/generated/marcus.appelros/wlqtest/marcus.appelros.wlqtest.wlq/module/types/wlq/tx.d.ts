import { Reader, Writer } from "protobufjs/minimal";
export declare const protobufPackage = "marcus.appelros.wlqtest.wlq";
export interface MsgJoinQueue {
    creator: string;
    tier: number;
    sizeId: number;
}
export interface MsgJoinQueueResponse {
}
export interface MsgSubmitMove {
    creator: string;
    gameId: number;
    x: number;
    y: number;
}
export interface MsgSubmitMoveResponse {
}
export interface MsgLeaveQueue {
    creator: string;
    tier: number;
    sizeId: number;
}
export interface MsgLeaveQueueResponse {
}
export declare const MsgJoinQueue: {
    encode(message: MsgJoinQueue, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): MsgJoinQueue;
    fromJSON(object: any): MsgJoinQueue;
    toJSON(message: MsgJoinQueue): unknown;
    fromPartial(object: DeepPartial<MsgJoinQueue>): MsgJoinQueue;
};
export declare const MsgJoinQueueResponse: {
    encode(_: MsgJoinQueueResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): MsgJoinQueueResponse;
    fromJSON(_: any): MsgJoinQueueResponse;
    toJSON(_: MsgJoinQueueResponse): unknown;
    fromPartial(_: DeepPartial<MsgJoinQueueResponse>): MsgJoinQueueResponse;
};
export declare const MsgSubmitMove: {
    encode(message: MsgSubmitMove, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): MsgSubmitMove;
    fromJSON(object: any): MsgSubmitMove;
    toJSON(message: MsgSubmitMove): unknown;
    fromPartial(object: DeepPartial<MsgSubmitMove>): MsgSubmitMove;
};
export declare const MsgSubmitMoveResponse: {
    encode(_: MsgSubmitMoveResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): MsgSubmitMoveResponse;
    fromJSON(_: any): MsgSubmitMoveResponse;
    toJSON(_: MsgSubmitMoveResponse): unknown;
    fromPartial(_: DeepPartial<MsgSubmitMoveResponse>): MsgSubmitMoveResponse;
};
export declare const MsgLeaveQueue: {
    encode(message: MsgLeaveQueue, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): MsgLeaveQueue;
    fromJSON(object: any): MsgLeaveQueue;
    toJSON(message: MsgLeaveQueue): unknown;
    fromPartial(object: DeepPartial<MsgLeaveQueue>): MsgLeaveQueue;
};
export declare const MsgLeaveQueueResponse: {
    encode(_: MsgLeaveQueueResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): MsgLeaveQueueResponse;
    fromJSON(_: any): MsgLeaveQueueResponse;
    toJSON(_: MsgLeaveQueueResponse): unknown;
    fromPartial(_: DeepPartial<MsgLeaveQueueResponse>): MsgLeaveQueueResponse;
};
/** Msg defines the Msg service. */
export interface Msg {
    JoinQueue(request: MsgJoinQueue): Promise<MsgJoinQueueResponse>;
    SubmitMove(request: MsgSubmitMove): Promise<MsgSubmitMoveResponse>;
    /** this line is used by starport scaffolding # proto/tx/rpc */
    LeaveQueue(request: MsgLeaveQueue): Promise<MsgLeaveQueueResponse>;
}
export declare class MsgClientImpl implements Msg {
    private readonly rpc;
    constructor(rpc: Rpc);
    JoinQueue(request: MsgJoinQueue): Promise<MsgJoinQueueResponse>;
    SubmitMove(request: MsgSubmitMove): Promise<MsgSubmitMoveResponse>;
    LeaveQueue(request: MsgLeaveQueue): Promise<MsgLeaveQueueResponse>;
}
interface Rpc {
    request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>;
}
declare type Builtin = Date | Function | Uint8Array | string | number | undefined;
export declare type DeepPartial<T> = T extends Builtin ? T : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>> : T extends {} ? {
    [K in keyof T]?: DeepPartial<T[K]>;
} : Partial<T>;
export {};
