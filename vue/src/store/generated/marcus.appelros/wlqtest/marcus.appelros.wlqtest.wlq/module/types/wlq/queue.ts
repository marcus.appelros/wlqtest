/* eslint-disable */
import * as Long from "long";
import { util, configure, Writer, Reader } from "protobufjs/minimal";

export const protobufPackage = "marcus.appelros.wlqtest.wlq";

export interface Queue {
  id: number;
  sizeId0: string[];
  sizeId1: string[];
  sizeId2: string[];
}

const baseQueue: object = { id: 0, sizeId0: "", sizeId1: "", sizeId2: "" };

export const Queue = {
  encode(message: Queue, writer: Writer = Writer.create()): Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint64(message.id);
    }
    for (const v of message.sizeId0) {
      writer.uint32(18).string(v!);
    }
    for (const v of message.sizeId1) {
      writer.uint32(26).string(v!);
    }
    for (const v of message.sizeId2) {
      writer.uint32(34).string(v!);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Queue {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueue } as Queue;
    message.sizeId0 = [];
    message.sizeId1 = [];
    message.sizeId2 = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = longToNumber(reader.uint64() as Long);
          break;
        case 2:
          message.sizeId0.push(reader.string());
          break;
        case 3:
          message.sizeId1.push(reader.string());
          break;
        case 4:
          message.sizeId2.push(reader.string());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Queue {
    const message = { ...baseQueue } as Queue;
    message.sizeId0 = [];
    message.sizeId1 = [];
    message.sizeId2 = [];
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    if (object.sizeId0 !== undefined && object.sizeId0 !== null) {
      for (const e of object.sizeId0) {
        message.sizeId0.push(String(e));
      }
    }
    if (object.sizeId1 !== undefined && object.sizeId1 !== null) {
      for (const e of object.sizeId1) {
        message.sizeId1.push(String(e));
      }
    }
    if (object.sizeId2 !== undefined && object.sizeId2 !== null) {
      for (const e of object.sizeId2) {
        message.sizeId2.push(String(e));
      }
    }
    return message;
  },

  toJSON(message: Queue): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    if (message.sizeId0) {
      obj.sizeId0 = message.sizeId0.map((e) => e);
    } else {
      obj.sizeId0 = [];
    }
    if (message.sizeId1) {
      obj.sizeId1 = message.sizeId1.map((e) => e);
    } else {
      obj.sizeId1 = [];
    }
    if (message.sizeId2) {
      obj.sizeId2 = message.sizeId2.map((e) => e);
    } else {
      obj.sizeId2 = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<Queue>): Queue {
    const message = { ...baseQueue } as Queue;
    message.sizeId0 = [];
    message.sizeId1 = [];
    message.sizeId2 = [];
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    if (object.sizeId0 !== undefined && object.sizeId0 !== null) {
      for (const e of object.sizeId0) {
        message.sizeId0.push(e);
      }
    }
    if (object.sizeId1 !== undefined && object.sizeId1 !== null) {
      for (const e of object.sizeId1) {
        message.sizeId1.push(e);
      }
    }
    if (object.sizeId2 !== undefined && object.sizeId2 !== null) {
      for (const e of object.sizeId2) {
        message.sizeId2.push(e);
      }
    }
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (util.Long !== Long) {
  util.Long = Long as any;
  configure();
}
