/* eslint-disable */
import * as Long from "long";
import { util, configure, Writer, Reader } from "protobufjs/minimal";

export const protobufPackage = "marcus.appelros.wlqtest.wlq";

export interface Game {
  id: number;
  tier: number;
  sizeId: number;
  players: string[];
  player: number;
  history: string;
  deadline: string;
  timeouts: number[];
  passes: number;
  next: number;
  prev: number;
  started: string;
  finished: string;
  areas: number[];
  bonds: number[];
  score: number;
  nfts: number[];
}

const baseGame: object = {
  id: 0,
  tier: 0,
  sizeId: 0,
  players: "",
  player: 0,
  history: "",
  deadline: "",
  timeouts: 0,
  passes: 0,
  next: 0,
  prev: 0,
  started: "",
  finished: "",
  areas: 0,
  bonds: 0,
  score: 0,
  nfts: 0,
};

export const Game = {
  encode(message: Game, writer: Writer = Writer.create()): Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint64(message.id);
    }
    if (message.tier !== 0) {
      writer.uint32(16).uint64(message.tier);
    }
    if (message.sizeId !== 0) {
      writer.uint32(24).uint64(message.sizeId);
    }
    for (const v of message.players) {
      writer.uint32(34).string(v!);
    }
    if (message.player !== 0) {
      writer.uint32(40).uint64(message.player);
    }
    if (message.history !== "") {
      writer.uint32(50).string(message.history);
    }
    if (message.deadline !== "") {
      writer.uint32(58).string(message.deadline);
    }
    writer.uint32(66).fork();
    for (const v of message.timeouts) {
      writer.uint64(v);
    }
    writer.ldelim();
    if (message.passes !== 0) {
      writer.uint32(72).uint64(message.passes);
    }
    if (message.next !== 0) {
      writer.uint32(80).uint64(message.next);
    }
    if (message.prev !== 0) {
      writer.uint32(88).uint64(message.prev);
    }
    if (message.started !== "") {
      writer.uint32(98).string(message.started);
    }
    if (message.finished !== "") {
      writer.uint32(106).string(message.finished);
    }
    writer.uint32(114).fork();
    for (const v of message.areas) {
      writer.uint64(v);
    }
    writer.ldelim();
    writer.uint32(122).fork();
    for (const v of message.bonds) {
      writer.uint64(v);
    }
    writer.ldelim();
    if (message.score !== 0) {
      writer.uint32(128).uint64(message.score);
    }
    writer.uint32(138).fork();
    for (const v of message.nfts) {
      writer.uint64(v);
    }
    writer.ldelim();
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Game {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGame } as Game;
    message.players = [];
    message.timeouts = [];
    message.areas = [];
    message.bonds = [];
    message.nfts = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = longToNumber(reader.uint64() as Long);
          break;
        case 2:
          message.tier = longToNumber(reader.uint64() as Long);
          break;
        case 3:
          message.sizeId = longToNumber(reader.uint64() as Long);
          break;
        case 4:
          message.players.push(reader.string());
          break;
        case 5:
          message.player = longToNumber(reader.uint64() as Long);
          break;
        case 6:
          message.history = reader.string();
          break;
        case 7:
          message.deadline = reader.string();
          break;
        case 8:
          if ((tag & 7) === 2) {
            const end2 = reader.uint32() + reader.pos;
            while (reader.pos < end2) {
              message.timeouts.push(longToNumber(reader.uint64() as Long));
            }
          } else {
            message.timeouts.push(longToNumber(reader.uint64() as Long));
          }
          break;
        case 9:
          message.passes = longToNumber(reader.uint64() as Long);
          break;
        case 10:
          message.next = longToNumber(reader.uint64() as Long);
          break;
        case 11:
          message.prev = longToNumber(reader.uint64() as Long);
          break;
        case 12:
          message.started = reader.string();
          break;
        case 13:
          message.finished = reader.string();
          break;
        case 14:
          if ((tag & 7) === 2) {
            const end2 = reader.uint32() + reader.pos;
            while (reader.pos < end2) {
              message.areas.push(longToNumber(reader.uint64() as Long));
            }
          } else {
            message.areas.push(longToNumber(reader.uint64() as Long));
          }
          break;
        case 15:
          if ((tag & 7) === 2) {
            const end2 = reader.uint32() + reader.pos;
            while (reader.pos < end2) {
              message.bonds.push(longToNumber(reader.uint64() as Long));
            }
          } else {
            message.bonds.push(longToNumber(reader.uint64() as Long));
          }
          break;
        case 16:
          message.score = longToNumber(reader.uint64() as Long);
          break;
        case 17:
          if ((tag & 7) === 2) {
            const end2 = reader.uint32() + reader.pos;
            while (reader.pos < end2) {
              message.nfts.push(longToNumber(reader.uint64() as Long));
            }
          } else {
            message.nfts.push(longToNumber(reader.uint64() as Long));
          }
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Game {
    const message = { ...baseGame } as Game;
    message.players = [];
    message.timeouts = [];
    message.areas = [];
    message.bonds = [];
    message.nfts = [];
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    if (object.tier !== undefined && object.tier !== null) {
      message.tier = Number(object.tier);
    } else {
      message.tier = 0;
    }
    if (object.sizeId !== undefined && object.sizeId !== null) {
      message.sizeId = Number(object.sizeId);
    } else {
      message.sizeId = 0;
    }
    if (object.players !== undefined && object.players !== null) {
      for (const e of object.players) {
        message.players.push(String(e));
      }
    }
    if (object.player !== undefined && object.player !== null) {
      message.player = Number(object.player);
    } else {
      message.player = 0;
    }
    if (object.history !== undefined && object.history !== null) {
      message.history = String(object.history);
    } else {
      message.history = "";
    }
    if (object.deadline !== undefined && object.deadline !== null) {
      message.deadline = String(object.deadline);
    } else {
      message.deadline = "";
    }
    if (object.timeouts !== undefined && object.timeouts !== null) {
      for (const e of object.timeouts) {
        message.timeouts.push(Number(e));
      }
    }
    if (object.passes !== undefined && object.passes !== null) {
      message.passes = Number(object.passes);
    } else {
      message.passes = 0;
    }
    if (object.next !== undefined && object.next !== null) {
      message.next = Number(object.next);
    } else {
      message.next = 0;
    }
    if (object.prev !== undefined && object.prev !== null) {
      message.prev = Number(object.prev);
    } else {
      message.prev = 0;
    }
    if (object.started !== undefined && object.started !== null) {
      message.started = String(object.started);
    } else {
      message.started = "";
    }
    if (object.finished !== undefined && object.finished !== null) {
      message.finished = String(object.finished);
    } else {
      message.finished = "";
    }
    if (object.areas !== undefined && object.areas !== null) {
      for (const e of object.areas) {
        message.areas.push(Number(e));
      }
    }
    if (object.bonds !== undefined && object.bonds !== null) {
      for (const e of object.bonds) {
        message.bonds.push(Number(e));
      }
    }
    if (object.score !== undefined && object.score !== null) {
      message.score = Number(object.score);
    } else {
      message.score = 0;
    }
    if (object.nfts !== undefined && object.nfts !== null) {
      for (const e of object.nfts) {
        message.nfts.push(Number(e));
      }
    }
    return message;
  },

  toJSON(message: Game): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.tier !== undefined && (obj.tier = message.tier);
    message.sizeId !== undefined && (obj.sizeId = message.sizeId);
    if (message.players) {
      obj.players = message.players.map((e) => e);
    } else {
      obj.players = [];
    }
    message.player !== undefined && (obj.player = message.player);
    message.history !== undefined && (obj.history = message.history);
    message.deadline !== undefined && (obj.deadline = message.deadline);
    if (message.timeouts) {
      obj.timeouts = message.timeouts.map((e) => e);
    } else {
      obj.timeouts = [];
    }
    message.passes !== undefined && (obj.passes = message.passes);
    message.next !== undefined && (obj.next = message.next);
    message.prev !== undefined && (obj.prev = message.prev);
    message.started !== undefined && (obj.started = message.started);
    message.finished !== undefined && (obj.finished = message.finished);
    if (message.areas) {
      obj.areas = message.areas.map((e) => e);
    } else {
      obj.areas = [];
    }
    if (message.bonds) {
      obj.bonds = message.bonds.map((e) => e);
    } else {
      obj.bonds = [];
    }
    message.score !== undefined && (obj.score = message.score);
    if (message.nfts) {
      obj.nfts = message.nfts.map((e) => e);
    } else {
      obj.nfts = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<Game>): Game {
    const message = { ...baseGame } as Game;
    message.players = [];
    message.timeouts = [];
    message.areas = [];
    message.bonds = [];
    message.nfts = [];
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    if (object.tier !== undefined && object.tier !== null) {
      message.tier = object.tier;
    } else {
      message.tier = 0;
    }
    if (object.sizeId !== undefined && object.sizeId !== null) {
      message.sizeId = object.sizeId;
    } else {
      message.sizeId = 0;
    }
    if (object.players !== undefined && object.players !== null) {
      for (const e of object.players) {
        message.players.push(e);
      }
    }
    if (object.player !== undefined && object.player !== null) {
      message.player = object.player;
    } else {
      message.player = 0;
    }
    if (object.history !== undefined && object.history !== null) {
      message.history = object.history;
    } else {
      message.history = "";
    }
    if (object.deadline !== undefined && object.deadline !== null) {
      message.deadline = object.deadline;
    } else {
      message.deadline = "";
    }
    if (object.timeouts !== undefined && object.timeouts !== null) {
      for (const e of object.timeouts) {
        message.timeouts.push(e);
      }
    }
    if (object.passes !== undefined && object.passes !== null) {
      message.passes = object.passes;
    } else {
      message.passes = 0;
    }
    if (object.next !== undefined && object.next !== null) {
      message.next = object.next;
    } else {
      message.next = 0;
    }
    if (object.prev !== undefined && object.prev !== null) {
      message.prev = object.prev;
    } else {
      message.prev = 0;
    }
    if (object.started !== undefined && object.started !== null) {
      message.started = object.started;
    } else {
      message.started = "";
    }
    if (object.finished !== undefined && object.finished !== null) {
      message.finished = object.finished;
    } else {
      message.finished = "";
    }
    if (object.areas !== undefined && object.areas !== null) {
      for (const e of object.areas) {
        message.areas.push(e);
      }
    }
    if (object.bonds !== undefined && object.bonds !== null) {
      for (const e of object.bonds) {
        message.bonds.push(e);
      }
    }
    if (object.score !== undefined && object.score !== null) {
      message.score = object.score;
    } else {
      message.score = 0;
    }
    if (object.nfts !== undefined && object.nfts !== null) {
      for (const e of object.nfts) {
        message.nfts.push(e);
      }
    }
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (util.Long !== Long) {
  util.Long = Long as any;
  configure();
}
