/* eslint-disable */
import { Reader, util, configure, Writer } from "protobufjs/minimal";
import * as Long from "long";
import { Params } from "../wlq/params";
import { Queue } from "../wlq/queue";
import {
  PageRequest,
  PageResponse,
} from "../cosmos/base/query/v1beta1/pagination";
import { Game } from "../wlq/game";

export const protobufPackage = "marcus.appelros.wlqtest.wlq";

/** QueryParamsRequest is request type for the Query/Params RPC method. */
export interface QueryParamsRequest {}

/** QueryParamsResponse is response type for the Query/Params RPC method. */
export interface QueryParamsResponse {
  /** params holds all the parameters of this module. */
  params: Params | undefined;
}

export interface QueryGetQueueRequest {
  id: number;
}

export interface QueryGetQueueResponse {
  Queue: Queue | undefined;
}

export interface QueryAllQueueRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllQueueResponse {
  Queue: Queue[];
  pagination: PageResponse | undefined;
}

export interface QueryGetGameRequest {
  id: number;
}

export interface QueryGetGameResponse {
  Game: Game | undefined;
}

export interface QueryAllGameRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllGameResponse {
  Game: Game[];
  pagination: PageResponse | undefined;
}

const baseQueryParamsRequest: object = {};

export const QueryParamsRequest = {
  encode(_: QueryParamsRequest, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryParamsRequest {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryParamsRequest } as QueryParamsRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): QueryParamsRequest {
    const message = { ...baseQueryParamsRequest } as QueryParamsRequest;
    return message;
  },

  toJSON(_: QueryParamsRequest): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<QueryParamsRequest>): QueryParamsRequest {
    const message = { ...baseQueryParamsRequest } as QueryParamsRequest;
    return message;
  },
};

const baseQueryParamsResponse: object = {};

export const QueryParamsResponse = {
  encode(
    message: QueryParamsResponse,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.params !== undefined) {
      Params.encode(message.params, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryParamsResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryParamsResponse } as QueryParamsResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.params = Params.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryParamsResponse {
    const message = { ...baseQueryParamsResponse } as QueryParamsResponse;
    if (object.params !== undefined && object.params !== null) {
      message.params = Params.fromJSON(object.params);
    } else {
      message.params = undefined;
    }
    return message;
  },

  toJSON(message: QueryParamsResponse): unknown {
    const obj: any = {};
    message.params !== undefined &&
      (obj.params = message.params ? Params.toJSON(message.params) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<QueryParamsResponse>): QueryParamsResponse {
    const message = { ...baseQueryParamsResponse } as QueryParamsResponse;
    if (object.params !== undefined && object.params !== null) {
      message.params = Params.fromPartial(object.params);
    } else {
      message.params = undefined;
    }
    return message;
  },
};

const baseQueryGetQueueRequest: object = { id: 0 };

export const QueryGetQueueRequest = {
  encode(
    message: QueryGetQueueRequest,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint64(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryGetQueueRequest {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryGetQueueRequest } as QueryGetQueueRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetQueueRequest {
    const message = { ...baseQueryGetQueueRequest } as QueryGetQueueRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    return message;
  },

  toJSON(message: QueryGetQueueRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<QueryGetQueueRequest>): QueryGetQueueRequest {
    const message = { ...baseQueryGetQueueRequest } as QueryGetQueueRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    return message;
  },
};

const baseQueryGetQueueResponse: object = {};

export const QueryGetQueueResponse = {
  encode(
    message: QueryGetQueueResponse,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.Queue !== undefined) {
      Queue.encode(message.Queue, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryGetQueueResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryGetQueueResponse } as QueryGetQueueResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Queue = Queue.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetQueueResponse {
    const message = { ...baseQueryGetQueueResponse } as QueryGetQueueResponse;
    if (object.Queue !== undefined && object.Queue !== null) {
      message.Queue = Queue.fromJSON(object.Queue);
    } else {
      message.Queue = undefined;
    }
    return message;
  },

  toJSON(message: QueryGetQueueResponse): unknown {
    const obj: any = {};
    message.Queue !== undefined &&
      (obj.Queue = message.Queue ? Queue.toJSON(message.Queue) : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QueryGetQueueResponse>
  ): QueryGetQueueResponse {
    const message = { ...baseQueryGetQueueResponse } as QueryGetQueueResponse;
    if (object.Queue !== undefined && object.Queue !== null) {
      message.Queue = Queue.fromPartial(object.Queue);
    } else {
      message.Queue = undefined;
    }
    return message;
  },
};

const baseQueryAllQueueRequest: object = {};

export const QueryAllQueueRequest = {
  encode(
    message: QueryAllQueueRequest,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryAllQueueRequest {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryAllQueueRequest } as QueryAllQueueRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllQueueRequest {
    const message = { ...baseQueryAllQueueRequest } as QueryAllQueueRequest;
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: QueryAllQueueRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined &&
      (obj.pagination = message.pagination
        ? PageRequest.toJSON(message.pagination)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<QueryAllQueueRequest>): QueryAllQueueRequest {
    const message = { ...baseQueryAllQueueRequest } as QueryAllQueueRequest;
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },
};

const baseQueryAllQueueResponse: object = {};

export const QueryAllQueueResponse = {
  encode(
    message: QueryAllQueueResponse,
    writer: Writer = Writer.create()
  ): Writer {
    for (const v of message.Queue) {
      Queue.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(
        message.pagination,
        writer.uint32(18).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryAllQueueResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryAllQueueResponse } as QueryAllQueueResponse;
    message.Queue = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Queue.push(Queue.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllQueueResponse {
    const message = { ...baseQueryAllQueueResponse } as QueryAllQueueResponse;
    message.Queue = [];
    if (object.Queue !== undefined && object.Queue !== null) {
      for (const e of object.Queue) {
        message.Queue.push(Queue.fromJSON(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: QueryAllQueueResponse): unknown {
    const obj: any = {};
    if (message.Queue) {
      obj.Queue = message.Queue.map((e) => (e ? Queue.toJSON(e) : undefined));
    } else {
      obj.Queue = [];
    }
    message.pagination !== undefined &&
      (obj.pagination = message.pagination
        ? PageResponse.toJSON(message.pagination)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QueryAllQueueResponse>
  ): QueryAllQueueResponse {
    const message = { ...baseQueryAllQueueResponse } as QueryAllQueueResponse;
    message.Queue = [];
    if (object.Queue !== undefined && object.Queue !== null) {
      for (const e of object.Queue) {
        message.Queue.push(Queue.fromPartial(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },
};

const baseQueryGetGameRequest: object = { id: 0 };

export const QueryGetGameRequest = {
  encode(
    message: QueryGetGameRequest,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint64(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryGetGameRequest {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryGetGameRequest } as QueryGetGameRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetGameRequest {
    const message = { ...baseQueryGetGameRequest } as QueryGetGameRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    return message;
  },

  toJSON(message: QueryGetGameRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<QueryGetGameRequest>): QueryGetGameRequest {
    const message = { ...baseQueryGetGameRequest } as QueryGetGameRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    return message;
  },
};

const baseQueryGetGameResponse: object = {};

export const QueryGetGameResponse = {
  encode(
    message: QueryGetGameResponse,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.Game !== undefined) {
      Game.encode(message.Game, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryGetGameResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryGetGameResponse } as QueryGetGameResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Game = Game.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetGameResponse {
    const message = { ...baseQueryGetGameResponse } as QueryGetGameResponse;
    if (object.Game !== undefined && object.Game !== null) {
      message.Game = Game.fromJSON(object.Game);
    } else {
      message.Game = undefined;
    }
    return message;
  },

  toJSON(message: QueryGetGameResponse): unknown {
    const obj: any = {};
    message.Game !== undefined &&
      (obj.Game = message.Game ? Game.toJSON(message.Game) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<QueryGetGameResponse>): QueryGetGameResponse {
    const message = { ...baseQueryGetGameResponse } as QueryGetGameResponse;
    if (object.Game !== undefined && object.Game !== null) {
      message.Game = Game.fromPartial(object.Game);
    } else {
      message.Game = undefined;
    }
    return message;
  },
};

const baseQueryAllGameRequest: object = {};

export const QueryAllGameRequest = {
  encode(
    message: QueryAllGameRequest,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryAllGameRequest {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryAllGameRequest } as QueryAllGameRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllGameRequest {
    const message = { ...baseQueryAllGameRequest } as QueryAllGameRequest;
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: QueryAllGameRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined &&
      (obj.pagination = message.pagination
        ? PageRequest.toJSON(message.pagination)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<QueryAllGameRequest>): QueryAllGameRequest {
    const message = { ...baseQueryAllGameRequest } as QueryAllGameRequest;
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },
};

const baseQueryAllGameResponse: object = {};

export const QueryAllGameResponse = {
  encode(
    message: QueryAllGameResponse,
    writer: Writer = Writer.create()
  ): Writer {
    for (const v of message.Game) {
      Game.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(
        message.pagination,
        writer.uint32(18).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryAllGameResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryAllGameResponse } as QueryAllGameResponse;
    message.Game = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Game.push(Game.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllGameResponse {
    const message = { ...baseQueryAllGameResponse } as QueryAllGameResponse;
    message.Game = [];
    if (object.Game !== undefined && object.Game !== null) {
      for (const e of object.Game) {
        message.Game.push(Game.fromJSON(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: QueryAllGameResponse): unknown {
    const obj: any = {};
    if (message.Game) {
      obj.Game = message.Game.map((e) => (e ? Game.toJSON(e) : undefined));
    } else {
      obj.Game = [];
    }
    message.pagination !== undefined &&
      (obj.pagination = message.pagination
        ? PageResponse.toJSON(message.pagination)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<QueryAllGameResponse>): QueryAllGameResponse {
    const message = { ...baseQueryAllGameResponse } as QueryAllGameResponse;
    message.Game = [];
    if (object.Game !== undefined && object.Game !== null) {
      for (const e of object.Game) {
        message.Game.push(Game.fromPartial(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },
};

/** Query defines the gRPC querier service. */
export interface Query {
  /** Parameters queries the parameters of the module. */
  Params(request: QueryParamsRequest): Promise<QueryParamsResponse>;
  /** Queries a Queue by id. */
  Queue(request: QueryGetQueueRequest): Promise<QueryGetQueueResponse>;
  /** Queries a list of Queue items. */
  QueueAll(request: QueryAllQueueRequest): Promise<QueryAllQueueResponse>;
  /** Queries a Game by id. */
  Game(request: QueryGetGameRequest): Promise<QueryGetGameResponse>;
  /** Queries a list of Game items. */
  GameAll(request: QueryAllGameRequest): Promise<QueryAllGameResponse>;
}

export class QueryClientImpl implements Query {
  private readonly rpc: Rpc;
  constructor(rpc: Rpc) {
    this.rpc = rpc;
  }
  Params(request: QueryParamsRequest): Promise<QueryParamsResponse> {
    const data = QueryParamsRequest.encode(request).finish();
    const promise = this.rpc.request(
      "marcus.appelros.wlqtest.wlq.Query",
      "Params",
      data
    );
    return promise.then((data) => QueryParamsResponse.decode(new Reader(data)));
  }

  Queue(request: QueryGetQueueRequest): Promise<QueryGetQueueResponse> {
    const data = QueryGetQueueRequest.encode(request).finish();
    const promise = this.rpc.request(
      "marcus.appelros.wlqtest.wlq.Query",
      "Queue",
      data
    );
    return promise.then((data) =>
      QueryGetQueueResponse.decode(new Reader(data))
    );
  }

  QueueAll(request: QueryAllQueueRequest): Promise<QueryAllQueueResponse> {
    const data = QueryAllQueueRequest.encode(request).finish();
    const promise = this.rpc.request(
      "marcus.appelros.wlqtest.wlq.Query",
      "QueueAll",
      data
    );
    return promise.then((data) =>
      QueryAllQueueResponse.decode(new Reader(data))
    );
  }

  Game(request: QueryGetGameRequest): Promise<QueryGetGameResponse> {
    const data = QueryGetGameRequest.encode(request).finish();
    const promise = this.rpc.request(
      "marcus.appelros.wlqtest.wlq.Query",
      "Game",
      data
    );
    return promise.then((data) =>
      QueryGetGameResponse.decode(new Reader(data))
    );
  }

  GameAll(request: QueryAllGameRequest): Promise<QueryAllGameResponse> {
    const data = QueryAllGameRequest.encode(request).finish();
    const promise = this.rpc.request(
      "marcus.appelros.wlqtest.wlq.Query",
      "GameAll",
      data
    );
    return promise.then((data) =>
      QueryAllGameResponse.decode(new Reader(data))
    );
  }
}

interface Rpc {
  request(
    service: string,
    method: string,
    data: Uint8Array
  ): Promise<Uint8Array>;
}

declare var self: any | undefined;
declare var window: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (util.Long !== Long) {
  util.Long = Long as any;
  configure();
}
