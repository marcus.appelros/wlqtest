import { Writer, Reader } from "protobufjs/minimal";
export declare const protobufPackage = "marcus.appelros.wlqtest.wlq";
export interface Game {
    id: number;
    tier: number;
    sizeId: number;
    players: string[];
    player: number;
    history: string;
    deadline: string;
    timeouts: number[];
    passes: number;
    next: number;
    prev: number;
    started: string;
    finished: string;
    areas: number[];
    bonds: number[];
    score: number;
    nfts: number[];
}
export declare const Game: {
    encode(message: Game, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): Game;
    fromJSON(object: any): Game;
    toJSON(message: Game): unknown;
    fromPartial(object: DeepPartial<Game>): Game;
};
declare type Builtin = Date | Function | Uint8Array | string | number | undefined;
export declare type DeepPartial<T> = T extends Builtin ? T : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>> : T extends {} ? {
    [K in keyof T]?: DeepPartial<T[K]>;
} : Partial<T>;
export {};
