import { Reader, Writer } from "protobufjs/minimal";
import { Params } from "../wlq/params";
import { Queue } from "../wlq/queue";
import { PageRequest, PageResponse } from "../cosmos/base/query/v1beta1/pagination";
import { Game } from "../wlq/game";
export declare const protobufPackage = "marcus.appelros.wlqtest.wlq";
/** QueryParamsRequest is request type for the Query/Params RPC method. */
export interface QueryParamsRequest {
}
/** QueryParamsResponse is response type for the Query/Params RPC method. */
export interface QueryParamsResponse {
    /** params holds all the parameters of this module. */
    params: Params | undefined;
}
export interface QueryGetQueueRequest {
    id: number;
}
export interface QueryGetQueueResponse {
    Queue: Queue | undefined;
}
export interface QueryAllQueueRequest {
    pagination: PageRequest | undefined;
}
export interface QueryAllQueueResponse {
    Queue: Queue[];
    pagination: PageResponse | undefined;
}
export interface QueryGetGameRequest {
    id: number;
}
export interface QueryGetGameResponse {
    Game: Game | undefined;
}
export interface QueryAllGameRequest {
    pagination: PageRequest | undefined;
}
export interface QueryAllGameResponse {
    Game: Game[];
    pagination: PageResponse | undefined;
}
export declare const QueryParamsRequest: {
    encode(_: QueryParamsRequest, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryParamsRequest;
    fromJSON(_: any): QueryParamsRequest;
    toJSON(_: QueryParamsRequest): unknown;
    fromPartial(_: DeepPartial<QueryParamsRequest>): QueryParamsRequest;
};
export declare const QueryParamsResponse: {
    encode(message: QueryParamsResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryParamsResponse;
    fromJSON(object: any): QueryParamsResponse;
    toJSON(message: QueryParamsResponse): unknown;
    fromPartial(object: DeepPartial<QueryParamsResponse>): QueryParamsResponse;
};
export declare const QueryGetQueueRequest: {
    encode(message: QueryGetQueueRequest, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryGetQueueRequest;
    fromJSON(object: any): QueryGetQueueRequest;
    toJSON(message: QueryGetQueueRequest): unknown;
    fromPartial(object: DeepPartial<QueryGetQueueRequest>): QueryGetQueueRequest;
};
export declare const QueryGetQueueResponse: {
    encode(message: QueryGetQueueResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryGetQueueResponse;
    fromJSON(object: any): QueryGetQueueResponse;
    toJSON(message: QueryGetQueueResponse): unknown;
    fromPartial(object: DeepPartial<QueryGetQueueResponse>): QueryGetQueueResponse;
};
export declare const QueryAllQueueRequest: {
    encode(message: QueryAllQueueRequest, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryAllQueueRequest;
    fromJSON(object: any): QueryAllQueueRequest;
    toJSON(message: QueryAllQueueRequest): unknown;
    fromPartial(object: DeepPartial<QueryAllQueueRequest>): QueryAllQueueRequest;
};
export declare const QueryAllQueueResponse: {
    encode(message: QueryAllQueueResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryAllQueueResponse;
    fromJSON(object: any): QueryAllQueueResponse;
    toJSON(message: QueryAllQueueResponse): unknown;
    fromPartial(object: DeepPartial<QueryAllQueueResponse>): QueryAllQueueResponse;
};
export declare const QueryGetGameRequest: {
    encode(message: QueryGetGameRequest, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryGetGameRequest;
    fromJSON(object: any): QueryGetGameRequest;
    toJSON(message: QueryGetGameRequest): unknown;
    fromPartial(object: DeepPartial<QueryGetGameRequest>): QueryGetGameRequest;
};
export declare const QueryGetGameResponse: {
    encode(message: QueryGetGameResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryGetGameResponse;
    fromJSON(object: any): QueryGetGameResponse;
    toJSON(message: QueryGetGameResponse): unknown;
    fromPartial(object: DeepPartial<QueryGetGameResponse>): QueryGetGameResponse;
};
export declare const QueryAllGameRequest: {
    encode(message: QueryAllGameRequest, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryAllGameRequest;
    fromJSON(object: any): QueryAllGameRequest;
    toJSON(message: QueryAllGameRequest): unknown;
    fromPartial(object: DeepPartial<QueryAllGameRequest>): QueryAllGameRequest;
};
export declare const QueryAllGameResponse: {
    encode(message: QueryAllGameResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryAllGameResponse;
    fromJSON(object: any): QueryAllGameResponse;
    toJSON(message: QueryAllGameResponse): unknown;
    fromPartial(object: DeepPartial<QueryAllGameResponse>): QueryAllGameResponse;
};
/** Query defines the gRPC querier service. */
export interface Query {
    /** Parameters queries the parameters of the module. */
    Params(request: QueryParamsRequest): Promise<QueryParamsResponse>;
    /** Queries a Queue by id. */
    Queue(request: QueryGetQueueRequest): Promise<QueryGetQueueResponse>;
    /** Queries a list of Queue items. */
    QueueAll(request: QueryAllQueueRequest): Promise<QueryAllQueueResponse>;
    /** Queries a Game by id. */
    Game(request: QueryGetGameRequest): Promise<QueryGetGameResponse>;
    /** Queries a list of Game items. */
    GameAll(request: QueryAllGameRequest): Promise<QueryAllGameResponse>;
}
export declare class QueryClientImpl implements Query {
    private readonly rpc;
    constructor(rpc: Rpc);
    Params(request: QueryParamsRequest): Promise<QueryParamsResponse>;
    Queue(request: QueryGetQueueRequest): Promise<QueryGetQueueResponse>;
    QueueAll(request: QueryAllQueueRequest): Promise<QueryAllQueueResponse>;
    Game(request: QueryGetGameRequest): Promise<QueryGetGameResponse>;
    GameAll(request: QueryAllGameRequest): Promise<QueryAllGameResponse>;
}
interface Rpc {
    request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>;
}
declare type Builtin = Date | Function | Uint8Array | string | number | undefined;
export declare type DeepPartial<T> = T extends Builtin ? T : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>> : T extends {} ? {
    [K in keyof T]?: DeepPartial<T[K]>;
} : Partial<T>;
export {};
