/* eslint-disable */
import * as Long from "long";
import { util, configure, Writer, Reader } from "protobufjs/minimal";
import { Params } from "../wlq/params";
import { Queue } from "../wlq/queue";
import { Game } from "../wlq/game";

export const protobufPackage = "marcus.appelros.wlqtest.wlq";

/** GenesisState defines the wlq module's genesis state. */
export interface GenesisState {
  params: Params | undefined;
  queueList: Queue[];
  queueCount: number;
  gameList: Game[];
  /** this line is used by starport scaffolding # genesis/proto/state */
  gameCount: number;
}

const baseGenesisState: object = { queueCount: 0, gameCount: 0 };

export const GenesisState = {
  encode(message: GenesisState, writer: Writer = Writer.create()): Writer {
    if (message.params !== undefined) {
      Params.encode(message.params, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.queueList) {
      Queue.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    if (message.queueCount !== 0) {
      writer.uint32(24).uint64(message.queueCount);
    }
    for (const v of message.gameList) {
      Game.encode(v!, writer.uint32(34).fork()).ldelim();
    }
    if (message.gameCount !== 0) {
      writer.uint32(40).uint64(message.gameCount);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): GenesisState {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGenesisState } as GenesisState;
    message.queueList = [];
    message.gameList = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.params = Params.decode(reader, reader.uint32());
          break;
        case 2:
          message.queueList.push(Queue.decode(reader, reader.uint32()));
          break;
        case 3:
          message.queueCount = longToNumber(reader.uint64() as Long);
          break;
        case 4:
          message.gameList.push(Game.decode(reader, reader.uint32()));
          break;
        case 5:
          message.gameCount = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GenesisState {
    const message = { ...baseGenesisState } as GenesisState;
    message.queueList = [];
    message.gameList = [];
    if (object.params !== undefined && object.params !== null) {
      message.params = Params.fromJSON(object.params);
    } else {
      message.params = undefined;
    }
    if (object.queueList !== undefined && object.queueList !== null) {
      for (const e of object.queueList) {
        message.queueList.push(Queue.fromJSON(e));
      }
    }
    if (object.queueCount !== undefined && object.queueCount !== null) {
      message.queueCount = Number(object.queueCount);
    } else {
      message.queueCount = 0;
    }
    if (object.gameList !== undefined && object.gameList !== null) {
      for (const e of object.gameList) {
        message.gameList.push(Game.fromJSON(e));
      }
    }
    if (object.gameCount !== undefined && object.gameCount !== null) {
      message.gameCount = Number(object.gameCount);
    } else {
      message.gameCount = 0;
    }
    return message;
  },

  toJSON(message: GenesisState): unknown {
    const obj: any = {};
    message.params !== undefined &&
      (obj.params = message.params ? Params.toJSON(message.params) : undefined);
    if (message.queueList) {
      obj.queueList = message.queueList.map((e) =>
        e ? Queue.toJSON(e) : undefined
      );
    } else {
      obj.queueList = [];
    }
    message.queueCount !== undefined && (obj.queueCount = message.queueCount);
    if (message.gameList) {
      obj.gameList = message.gameList.map((e) =>
        e ? Game.toJSON(e) : undefined
      );
    } else {
      obj.gameList = [];
    }
    message.gameCount !== undefined && (obj.gameCount = message.gameCount);
    return obj;
  },

  fromPartial(object: DeepPartial<GenesisState>): GenesisState {
    const message = { ...baseGenesisState } as GenesisState;
    message.queueList = [];
    message.gameList = [];
    if (object.params !== undefined && object.params !== null) {
      message.params = Params.fromPartial(object.params);
    } else {
      message.params = undefined;
    }
    if (object.queueList !== undefined && object.queueList !== null) {
      for (const e of object.queueList) {
        message.queueList.push(Queue.fromPartial(e));
      }
    }
    if (object.queueCount !== undefined && object.queueCount !== null) {
      message.queueCount = object.queueCount;
    } else {
      message.queueCount = 0;
    }
    if (object.gameList !== undefined && object.gameList !== null) {
      for (const e of object.gameList) {
        message.gameList.push(Game.fromPartial(e));
      }
    }
    if (object.gameCount !== undefined && object.gameCount !== null) {
      message.gameCount = object.gameCount;
    } else {
      message.gameCount = 0;
    }
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (util.Long !== Long) {
  util.Long = Long as any;
  configure();
}
