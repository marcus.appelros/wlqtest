/* eslint-disable */
import { Reader, util, configure, Writer } from "protobufjs/minimal";
import * as Long from "long";
export const protobufPackage = "marcus.appelros.wlqtest.wlq";
const baseMsgJoinQueue = { creator: "", tier: 0, sizeId: 0 };
export const MsgJoinQueue = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== "") {
            writer.uint32(10).string(message.creator);
        }
        if (message.tier !== 0) {
            writer.uint32(16).uint64(message.tier);
        }
        if (message.sizeId !== 0) {
            writer.uint32(24).uint64(message.sizeId);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgJoinQueue };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.tier = longToNumber(reader.uint64());
                    break;
                case 3:
                    message.sizeId = longToNumber(reader.uint64());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgJoinQueue };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = "";
        }
        if (object.tier !== undefined && object.tier !== null) {
            message.tier = Number(object.tier);
        }
        else {
            message.tier = 0;
        }
        if (object.sizeId !== undefined && object.sizeId !== null) {
            message.sizeId = Number(object.sizeId);
        }
        else {
            message.sizeId = 0;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.tier !== undefined && (obj.tier = message.tier);
        message.sizeId !== undefined && (obj.sizeId = message.sizeId);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgJoinQueue };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = "";
        }
        if (object.tier !== undefined && object.tier !== null) {
            message.tier = object.tier;
        }
        else {
            message.tier = 0;
        }
        if (object.sizeId !== undefined && object.sizeId !== null) {
            message.sizeId = object.sizeId;
        }
        else {
            message.sizeId = 0;
        }
        return message;
    },
};
const baseMsgJoinQueueResponse = {};
export const MsgJoinQueueResponse = {
    encode(_, writer = Writer.create()) {
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgJoinQueueResponse };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(_) {
        const message = { ...baseMsgJoinQueueResponse };
        return message;
    },
    toJSON(_) {
        const obj = {};
        return obj;
    },
    fromPartial(_) {
        const message = { ...baseMsgJoinQueueResponse };
        return message;
    },
};
const baseMsgSubmitMove = { creator: "", gameId: 0, x: 0, y: 0 };
export const MsgSubmitMove = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== "") {
            writer.uint32(10).string(message.creator);
        }
        if (message.gameId !== 0) {
            writer.uint32(16).uint64(message.gameId);
        }
        if (message.x !== 0) {
            writer.uint32(24).int32(message.x);
        }
        if (message.y !== 0) {
            writer.uint32(32).int32(message.y);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgSubmitMove };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.gameId = longToNumber(reader.uint64());
                    break;
                case 3:
                    message.x = reader.int32();
                    break;
                case 4:
                    message.y = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgSubmitMove };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = "";
        }
        if (object.gameId !== undefined && object.gameId !== null) {
            message.gameId = Number(object.gameId);
        }
        else {
            message.gameId = 0;
        }
        if (object.x !== undefined && object.x !== null) {
            message.x = Number(object.x);
        }
        else {
            message.x = 0;
        }
        if (object.y !== undefined && object.y !== null) {
            message.y = Number(object.y);
        }
        else {
            message.y = 0;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.gameId !== undefined && (obj.gameId = message.gameId);
        message.x !== undefined && (obj.x = message.x);
        message.y !== undefined && (obj.y = message.y);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgSubmitMove };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = "";
        }
        if (object.gameId !== undefined && object.gameId !== null) {
            message.gameId = object.gameId;
        }
        else {
            message.gameId = 0;
        }
        if (object.x !== undefined && object.x !== null) {
            message.x = object.x;
        }
        else {
            message.x = 0;
        }
        if (object.y !== undefined && object.y !== null) {
            message.y = object.y;
        }
        else {
            message.y = 0;
        }
        return message;
    },
};
const baseMsgSubmitMoveResponse = {};
export const MsgSubmitMoveResponse = {
    encode(_, writer = Writer.create()) {
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgSubmitMoveResponse };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(_) {
        const message = { ...baseMsgSubmitMoveResponse };
        return message;
    },
    toJSON(_) {
        const obj = {};
        return obj;
    },
    fromPartial(_) {
        const message = { ...baseMsgSubmitMoveResponse };
        return message;
    },
};
const baseMsgLeaveQueue = { creator: "", tier: 0, sizeId: 0 };
export const MsgLeaveQueue = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== "") {
            writer.uint32(10).string(message.creator);
        }
        if (message.tier !== 0) {
            writer.uint32(16).uint64(message.tier);
        }
        if (message.sizeId !== 0) {
            writer.uint32(24).uint64(message.sizeId);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgLeaveQueue };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.tier = longToNumber(reader.uint64());
                    break;
                case 3:
                    message.sizeId = longToNumber(reader.uint64());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgLeaveQueue };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = "";
        }
        if (object.tier !== undefined && object.tier !== null) {
            message.tier = Number(object.tier);
        }
        else {
            message.tier = 0;
        }
        if (object.sizeId !== undefined && object.sizeId !== null) {
            message.sizeId = Number(object.sizeId);
        }
        else {
            message.sizeId = 0;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.tier !== undefined && (obj.tier = message.tier);
        message.sizeId !== undefined && (obj.sizeId = message.sizeId);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgLeaveQueue };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = "";
        }
        if (object.tier !== undefined && object.tier !== null) {
            message.tier = object.tier;
        }
        else {
            message.tier = 0;
        }
        if (object.sizeId !== undefined && object.sizeId !== null) {
            message.sizeId = object.sizeId;
        }
        else {
            message.sizeId = 0;
        }
        return message;
    },
};
const baseMsgLeaveQueueResponse = {};
export const MsgLeaveQueueResponse = {
    encode(_, writer = Writer.create()) {
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgLeaveQueueResponse };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(_) {
        const message = { ...baseMsgLeaveQueueResponse };
        return message;
    },
    toJSON(_) {
        const obj = {};
        return obj;
    },
    fromPartial(_) {
        const message = { ...baseMsgLeaveQueueResponse };
        return message;
    },
};
export class MsgClientImpl {
    constructor(rpc) {
        this.rpc = rpc;
    }
    JoinQueue(request) {
        const data = MsgJoinQueue.encode(request).finish();
        const promise = this.rpc.request("marcus.appelros.wlqtest.wlq.Msg", "JoinQueue", data);
        return promise.then((data) => MsgJoinQueueResponse.decode(new Reader(data)));
    }
    SubmitMove(request) {
        const data = MsgSubmitMove.encode(request).finish();
        const promise = this.rpc.request("marcus.appelros.wlqtest.wlq.Msg", "SubmitMove", data);
        return promise.then((data) => MsgSubmitMoveResponse.decode(new Reader(data)));
    }
    LeaveQueue(request) {
        const data = MsgLeaveQueue.encode(request).finish();
        const promise = this.rpc.request("marcus.appelros.wlqtest.wlq.Msg", "LeaveQueue", data);
        return promise.then((data) => MsgLeaveQueueResponse.decode(new Reader(data)));
    }
}
var globalThis = (() => {
    if (typeof globalThis !== "undefined")
        return globalThis;
    if (typeof self !== "undefined")
        return self;
    if (typeof window !== "undefined")
        return window;
    if (typeof global !== "undefined")
        return global;
    throw "Unable to locate global object";
})();
function longToNumber(long) {
    if (long.gt(Number.MAX_SAFE_INTEGER)) {
        throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
    }
    return long.toNumber();
}
if (util.Long !== Long) {
    util.Long = Long;
    configure();
}
