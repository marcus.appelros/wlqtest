/* eslint-disable */
import { Reader, util, configure, Writer } from "protobufjs/minimal";
import * as Long from "long";

export const protobufPackage = "marcus.appelros.wlqtest.wlq";

export interface MsgJoinQueue {
  creator: string;
  tier: number;
  sizeId: number;
}

export interface MsgJoinQueueResponse {}

export interface MsgSubmitMove {
  creator: string;
  gameId: number;
  x: number;
  y: number;
}

export interface MsgSubmitMoveResponse {}

export interface MsgLeaveQueue {
  creator: string;
  tier: number;
  sizeId: number;
}

export interface MsgLeaveQueueResponse {}

const baseMsgJoinQueue: object = { creator: "", tier: 0, sizeId: 0 };

export const MsgJoinQueue = {
  encode(message: MsgJoinQueue, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tier !== 0) {
      writer.uint32(16).uint64(message.tier);
    }
    if (message.sizeId !== 0) {
      writer.uint32(24).uint64(message.sizeId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgJoinQueue {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgJoinQueue } as MsgJoinQueue;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tier = longToNumber(reader.uint64() as Long);
          break;
        case 3:
          message.sizeId = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgJoinQueue {
    const message = { ...baseMsgJoinQueue } as MsgJoinQueue;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.tier !== undefined && object.tier !== null) {
      message.tier = Number(object.tier);
    } else {
      message.tier = 0;
    }
    if (object.sizeId !== undefined && object.sizeId !== null) {
      message.sizeId = Number(object.sizeId);
    } else {
      message.sizeId = 0;
    }
    return message;
  },

  toJSON(message: MsgJoinQueue): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tier !== undefined && (obj.tier = message.tier);
    message.sizeId !== undefined && (obj.sizeId = message.sizeId);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgJoinQueue>): MsgJoinQueue {
    const message = { ...baseMsgJoinQueue } as MsgJoinQueue;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.tier !== undefined && object.tier !== null) {
      message.tier = object.tier;
    } else {
      message.tier = 0;
    }
    if (object.sizeId !== undefined && object.sizeId !== null) {
      message.sizeId = object.sizeId;
    } else {
      message.sizeId = 0;
    }
    return message;
  },
};

const baseMsgJoinQueueResponse: object = {};

export const MsgJoinQueueResponse = {
  encode(_: MsgJoinQueueResponse, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgJoinQueueResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgJoinQueueResponse } as MsgJoinQueueResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgJoinQueueResponse {
    const message = { ...baseMsgJoinQueueResponse } as MsgJoinQueueResponse;
    return message;
  },

  toJSON(_: MsgJoinQueueResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<MsgJoinQueueResponse>): MsgJoinQueueResponse {
    const message = { ...baseMsgJoinQueueResponse } as MsgJoinQueueResponse;
    return message;
  },
};

const baseMsgSubmitMove: object = { creator: "", gameId: 0, x: 0, y: 0 };

export const MsgSubmitMove = {
  encode(message: MsgSubmitMove, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.gameId !== 0) {
      writer.uint32(16).uint64(message.gameId);
    }
    if (message.x !== 0) {
      writer.uint32(24).int32(message.x);
    }
    if (message.y !== 0) {
      writer.uint32(32).int32(message.y);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgSubmitMove {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgSubmitMove } as MsgSubmitMove;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.gameId = longToNumber(reader.uint64() as Long);
          break;
        case 3:
          message.x = reader.int32();
          break;
        case 4:
          message.y = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgSubmitMove {
    const message = { ...baseMsgSubmitMove } as MsgSubmitMove;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.gameId !== undefined && object.gameId !== null) {
      message.gameId = Number(object.gameId);
    } else {
      message.gameId = 0;
    }
    if (object.x !== undefined && object.x !== null) {
      message.x = Number(object.x);
    } else {
      message.x = 0;
    }
    if (object.y !== undefined && object.y !== null) {
      message.y = Number(object.y);
    } else {
      message.y = 0;
    }
    return message;
  },

  toJSON(message: MsgSubmitMove): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.gameId !== undefined && (obj.gameId = message.gameId);
    message.x !== undefined && (obj.x = message.x);
    message.y !== undefined && (obj.y = message.y);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgSubmitMove>): MsgSubmitMove {
    const message = { ...baseMsgSubmitMove } as MsgSubmitMove;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.gameId !== undefined && object.gameId !== null) {
      message.gameId = object.gameId;
    } else {
      message.gameId = 0;
    }
    if (object.x !== undefined && object.x !== null) {
      message.x = object.x;
    } else {
      message.x = 0;
    }
    if (object.y !== undefined && object.y !== null) {
      message.y = object.y;
    } else {
      message.y = 0;
    }
    return message;
  },
};

const baseMsgSubmitMoveResponse: object = {};

export const MsgSubmitMoveResponse = {
  encode(_: MsgSubmitMoveResponse, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgSubmitMoveResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgSubmitMoveResponse } as MsgSubmitMoveResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgSubmitMoveResponse {
    const message = { ...baseMsgSubmitMoveResponse } as MsgSubmitMoveResponse;
    return message;
  },

  toJSON(_: MsgSubmitMoveResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<MsgSubmitMoveResponse>): MsgSubmitMoveResponse {
    const message = { ...baseMsgSubmitMoveResponse } as MsgSubmitMoveResponse;
    return message;
  },
};

const baseMsgLeaveQueue: object = { creator: "", tier: 0, sizeId: 0 };

export const MsgLeaveQueue = {
  encode(message: MsgLeaveQueue, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tier !== 0) {
      writer.uint32(16).uint64(message.tier);
    }
    if (message.sizeId !== 0) {
      writer.uint32(24).uint64(message.sizeId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgLeaveQueue {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgLeaveQueue } as MsgLeaveQueue;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tier = longToNumber(reader.uint64() as Long);
          break;
        case 3:
          message.sizeId = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgLeaveQueue {
    const message = { ...baseMsgLeaveQueue } as MsgLeaveQueue;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.tier !== undefined && object.tier !== null) {
      message.tier = Number(object.tier);
    } else {
      message.tier = 0;
    }
    if (object.sizeId !== undefined && object.sizeId !== null) {
      message.sizeId = Number(object.sizeId);
    } else {
      message.sizeId = 0;
    }
    return message;
  },

  toJSON(message: MsgLeaveQueue): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tier !== undefined && (obj.tier = message.tier);
    message.sizeId !== undefined && (obj.sizeId = message.sizeId);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgLeaveQueue>): MsgLeaveQueue {
    const message = { ...baseMsgLeaveQueue } as MsgLeaveQueue;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.tier !== undefined && object.tier !== null) {
      message.tier = object.tier;
    } else {
      message.tier = 0;
    }
    if (object.sizeId !== undefined && object.sizeId !== null) {
      message.sizeId = object.sizeId;
    } else {
      message.sizeId = 0;
    }
    return message;
  },
};

const baseMsgLeaveQueueResponse: object = {};

export const MsgLeaveQueueResponse = {
  encode(_: MsgLeaveQueueResponse, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgLeaveQueueResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgLeaveQueueResponse } as MsgLeaveQueueResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgLeaveQueueResponse {
    const message = { ...baseMsgLeaveQueueResponse } as MsgLeaveQueueResponse;
    return message;
  },

  toJSON(_: MsgLeaveQueueResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<MsgLeaveQueueResponse>): MsgLeaveQueueResponse {
    const message = { ...baseMsgLeaveQueueResponse } as MsgLeaveQueueResponse;
    return message;
  },
};

/** Msg defines the Msg service. */
export interface Msg {
  JoinQueue(request: MsgJoinQueue): Promise<MsgJoinQueueResponse>;
  SubmitMove(request: MsgSubmitMove): Promise<MsgSubmitMoveResponse>;
  /** this line is used by starport scaffolding # proto/tx/rpc */
  LeaveQueue(request: MsgLeaveQueue): Promise<MsgLeaveQueueResponse>;
}

export class MsgClientImpl implements Msg {
  private readonly rpc: Rpc;
  constructor(rpc: Rpc) {
    this.rpc = rpc;
  }
  JoinQueue(request: MsgJoinQueue): Promise<MsgJoinQueueResponse> {
    const data = MsgJoinQueue.encode(request).finish();
    const promise = this.rpc.request(
      "marcus.appelros.wlqtest.wlq.Msg",
      "JoinQueue",
      data
    );
    return promise.then((data) =>
      MsgJoinQueueResponse.decode(new Reader(data))
    );
  }

  SubmitMove(request: MsgSubmitMove): Promise<MsgSubmitMoveResponse> {
    const data = MsgSubmitMove.encode(request).finish();
    const promise = this.rpc.request(
      "marcus.appelros.wlqtest.wlq.Msg",
      "SubmitMove",
      data
    );
    return promise.then((data) =>
      MsgSubmitMoveResponse.decode(new Reader(data))
    );
  }

  LeaveQueue(request: MsgLeaveQueue): Promise<MsgLeaveQueueResponse> {
    const data = MsgLeaveQueue.encode(request).finish();
    const promise = this.rpc.request(
      "marcus.appelros.wlqtest.wlq.Msg",
      "LeaveQueue",
      data
    );
    return promise.then((data) =>
      MsgLeaveQueueResponse.decode(new Reader(data))
    );
  }
}

interface Rpc {
  request(
    service: string,
    method: string,
    data: Uint8Array
  ): Promise<Uint8Array>;
}

declare var self: any | undefined;
declare var window: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (util.Long !== Long) {
  util.Long = Long as any;
  configure();
}
