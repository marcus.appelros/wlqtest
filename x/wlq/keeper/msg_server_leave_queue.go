package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"gitlab.com/marcus.appelros/wlqtest/x/wlq/types"
)

func (k msgServer) LeaveQueue(goCtx context.Context, msg *types.MsgLeaveQueue) (*types.MsgLeaveQueueResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	queue, found := k.GetQueue(ctx, msg.Tier)
	if !found {
		panic("Queue not found")
	}
	var q *[]string
	if msg.SizeId == 0 {
		q = &queue.SizeId0
	} else if msg.SizeId == 1 {
		q = &queue.SizeId1
	} else if msg.SizeId == 2 {
		q = &queue.SizeId2
	} else {
		panic("Invalid sizeId")
	}
	for i, waiting := range *q {
		if waiting == msg.Creator {
			*q = append((*q)[:i], (*q)[i+1:]...)
			k.SetQueue(ctx, queue)
			return &types.MsgLeaveQueueResponse{}, nil
		}
	}
	
	return nil, types.ErrNotInQueue
}
