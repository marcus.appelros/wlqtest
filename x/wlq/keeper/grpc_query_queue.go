package keeper

import (
	"context"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/cosmos/cosmos-sdk/types/query"
	"gitlab.com/marcus.appelros/wlqtest/x/wlq/types"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (k Keeper) QueueAll(c context.Context, req *types.QueryAllQueueRequest) (*types.QueryAllQueueResponse, error) {
	if req == nil {
		return nil, status.Error(codes.InvalidArgument, "invalid request")
	}

	var queues []types.Queue
	ctx := sdk.UnwrapSDKContext(c)

	store := ctx.KVStore(k.storeKey)
	queueStore := prefix.NewStore(store, types.KeyPrefix(types.QueueKey))

	pageRes, err := query.Paginate(queueStore, req.Pagination, func(key []byte, value []byte) error {
		var queue types.Queue
		if err := k.cdc.Unmarshal(value, &queue); err != nil {
			return err
		}

		queues = append(queues, queue)
		return nil
	})

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.QueryAllQueueResponse{Queue: queues, Pagination: pageRes}, nil
}

func (k Keeper) Queue(c context.Context, req *types.QueryGetQueueRequest) (*types.QueryGetQueueResponse, error) {
	if req == nil {
		return nil, status.Error(codes.InvalidArgument, "invalid request")
	}

	ctx := sdk.UnwrapSDKContext(c)
	queue, found := k.GetQueue(ctx, req.Id)
	if !found {
		return nil, sdkerrors.ErrKeyNotFound
	}

	return &types.QueryGetQueueResponse{Queue: queue}, nil
}
