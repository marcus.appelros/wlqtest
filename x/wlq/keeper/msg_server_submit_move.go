package keeper

import (
	"context"
	"fmt"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"gitlab.com/marcus.appelros/wlqtest/x/wlq/board"
	"gitlab.com/marcus.appelros/wlqtest/x/wlq/types"
)

func (k msgServer) SubmitMove(goCtx context.Context, msg *types.MsgSubmitMove) (*types.MsgSubmitMoveResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	game, found := k.GetGame(ctx, msg.GameId)
	if !found {
		return nil, types.ErrGameNotFound
	}
	if game.Finished != "" {
		return nil, types.ErrGameFinished
	}
	if msg.Creator != game.Players[game.Player] {
		return nil, types.ErrNotYourTurn
	}
	v := board.ValidateMove(game.History, int(msg.X), int(msg.Y))
	if v == 0 {
		game.History += fmt.Sprintf("%v,%v;", msg.X, msg.Y)
		game.Passes = 0
	} else if v == 1 {
		game.History += "999,0;"
		game.Passes += 1
	} else if v == 2 {
		return nil, types.ErrOccupied
	} else {
		panic("ValidateMove failed")
	}
	if game.Passes > 2 {
		b, err := board.LoadHistory(game.History)
		if err != nil {
			panic(err)
		}
		as, bs, s := b.Score()
		game.Areas = as
		game.Bonds = bs
		game.Score = s
		game.Finished = ctx.BlockTime().Format(types.TimeLayout)
	} else {
		game.Player = (game.Player + 1) % 3
		game.Deadline = ctx.BlockTime().Add(types.MaxTurnDuration).UTC().Format(types.TimeLayout)
	}
	k.SetGame(ctx, game)

	return &types.MsgSubmitMoveResponse{}, nil
}
