package keeper

import (
	"context"
	"fmt"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"gitlab.com/marcus.appelros/wlqtest/x/wlq/types"
)

func (k msgServer) JoinQueue(goCtx context.Context, msg *types.MsgJoinQueue) (*types.MsgJoinQueueResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	queue, found := k.GetQueue(ctx, msg.Tier)
	if !found {
		panic("Queue not found")
	}
	var q *[]string
	if msg.SizeId == 0 {
		q = &queue.SizeId0
	} else if msg.SizeId == 1 {
		q = &queue.SizeId1
	} else if msg.SizeId == 2 {
		q = &queue.SizeId2
	} else {
		panic("Invalid sizeId")
	}
	present := false
	for _, waiting := range *q {
		if waiting == msg.Creator {
			present = true
			break
		}
	}
	if present {
		return nil, types.ErrAlreadyInQueue
	}
	*q = append(*q, msg.Creator)

	//The game starting logic will be moved to endblock
	if len(*q) > 2 {
		var game = types.Game{
			Tier:     msg.Tier,
			SizeId:   msg.SizeId,
			Players:  []string{(*q)[2], (*q)[1], (*q)[0]},
			Player:   0,
			History:  fmt.Sprintf("%v;", types.Sizes[msg.SizeId]),
			Deadline: ctx.BlockTime().Add(types.MaxTurnDuration).UTC().Format(types.TimeLayout),
			Started:  ctx.BlockTime().Format(types.TimeLayout),
		}
		k.AppendGame(ctx, game)
		*q = []string{}
	}

	k.SetQueue(ctx, queue)

	return &types.MsgJoinQueueResponse{}, nil
}
