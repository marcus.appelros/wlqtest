package keeper_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	testkeeper "gitlab.com/marcus.appelros/wlqtest/testutil/keeper"
	"gitlab.com/marcus.appelros/wlqtest/x/wlq/types"
)

func TestGetParams(t *testing.T) {
	k, ctx := testkeeper.WlqKeeper(t)
	params := types.DefaultParams()

	k.SetParams(ctx, params)

	require.EqualValues(t, params, k.GetParams(ctx))
}
