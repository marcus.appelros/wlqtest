package keeper_test

import (
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/cosmos/cosmos-sdk/types/query"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	keepertest "gitlab.com/marcus.appelros/wlqtest/testutil/keeper"
	"gitlab.com/marcus.appelros/wlqtest/testutil/nullify"
	"gitlab.com/marcus.appelros/wlqtest/x/wlq/types"
)

func TestQueueQuerySingle(t *testing.T) {
	keeper, ctx := keepertest.WlqKeeper(t)
	wctx := sdk.WrapSDKContext(ctx)
	msgs := createNQueue(keeper, ctx, 2)
	for _, tc := range []struct {
		desc     string
		request  *types.QueryGetQueueRequest
		response *types.QueryGetQueueResponse
		err      error
	}{
		{
			desc:     "First",
			request:  &types.QueryGetQueueRequest{Id: msgs[0].Id},
			response: &types.QueryGetQueueResponse{Queue: msgs[0]},
		},
		{
			desc:     "Second",
			request:  &types.QueryGetQueueRequest{Id: msgs[1].Id},
			response: &types.QueryGetQueueResponse{Queue: msgs[1]},
		},
		{
			desc:    "KeyNotFound",
			request: &types.QueryGetQueueRequest{Id: uint64(len(msgs))},
			err:     sdkerrors.ErrKeyNotFound,
		},
		{
			desc: "InvalidRequest",
			err:  status.Error(codes.InvalidArgument, "invalid request"),
		},
	} {
		t.Run(tc.desc, func(t *testing.T) {
			response, err := keeper.Queue(wctx, tc.request)
			if tc.err != nil {
				require.ErrorIs(t, err, tc.err)
			} else {
				require.NoError(t, err)
				require.Equal(t,
					nullify.Fill(tc.response),
					nullify.Fill(response),
				)
			}
		})
	}
}

func TestQueueQueryPaginated(t *testing.T) {
	keeper, ctx := keepertest.WlqKeeper(t)
	wctx := sdk.WrapSDKContext(ctx)
	msgs := createNQueue(keeper, ctx, 5)

	request := func(next []byte, offset, limit uint64, total bool) *types.QueryAllQueueRequest {
		return &types.QueryAllQueueRequest{
			Pagination: &query.PageRequest{
				Key:        next,
				Offset:     offset,
				Limit:      limit,
				CountTotal: total,
			},
		}
	}
	t.Run("ByOffset", func(t *testing.T) {
		step := 2
		for i := 0; i < len(msgs); i += step {
			resp, err := keeper.QueueAll(wctx, request(nil, uint64(i), uint64(step), false))
			require.NoError(t, err)
			require.LessOrEqual(t, len(resp.Queue), step)
			require.Subset(t,
				nullify.Fill(msgs),
				nullify.Fill(resp.Queue),
			)
		}
	})
	t.Run("ByKey", func(t *testing.T) {
		step := 2
		var next []byte
		for i := 0; i < len(msgs); i += step {
			resp, err := keeper.QueueAll(wctx, request(next, 0, uint64(step), false))
			require.NoError(t, err)
			require.LessOrEqual(t, len(resp.Queue), step)
			require.Subset(t,
				nullify.Fill(msgs),
				nullify.Fill(resp.Queue),
			)
			next = resp.Pagination.NextKey
		}
	})
	t.Run("Total", func(t *testing.T) {
		resp, err := keeper.QueueAll(wctx, request(nil, 0, 0, true))
		require.NoError(t, err)
		require.Equal(t, len(msgs), int(resp.Pagination.Total))
		require.ElementsMatch(t,
			nullify.Fill(msgs),
			nullify.Fill(resp.Queue),
		)
	})
	t.Run("InvalidRequest", func(t *testing.T) {
		_, err := keeper.QueueAll(wctx, nil)
		require.ErrorIs(t, err, status.Error(codes.InvalidArgument, "invalid request"))
	})
}
