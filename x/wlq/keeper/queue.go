package keeper

import (
	"encoding/binary"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"gitlab.com/marcus.appelros/wlqtest/x/wlq/types"
)

// GetQueueCount get the total number of queue
func (k Keeper) GetQueueCount(ctx sdk.Context) uint64 {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), []byte{})
	byteKey := types.KeyPrefix(types.QueueCountKey)
	bz := store.Get(byteKey)

	// Count doesn't exist: no element
	if bz == nil {
		return 0
	}

	// Parse bytes
	return binary.BigEndian.Uint64(bz)
}

// SetQueueCount set the total number of queue
func (k Keeper) SetQueueCount(ctx sdk.Context, count uint64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), []byte{})
	byteKey := types.KeyPrefix(types.QueueCountKey)
	bz := make([]byte, 8)
	binary.BigEndian.PutUint64(bz, count)
	store.Set(byteKey, bz)
}

// AppendQueue appends a queue in the store with a new id and update the count
func (k Keeper) AppendQueue(
	ctx sdk.Context,
	queue types.Queue,
) uint64 {
	// Create the queue
	count := k.GetQueueCount(ctx)

	// Set the ID of the appended value
	queue.Id = count

	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.QueueKey))
	appendedValue := k.cdc.MustMarshal(&queue)
	store.Set(GetQueueIDBytes(queue.Id), appendedValue)

	// Update queue count
	k.SetQueueCount(ctx, count+1)

	return count
}

// SetQueue set a specific queue in the store
func (k Keeper) SetQueue(ctx sdk.Context, queue types.Queue) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.QueueKey))
	b := k.cdc.MustMarshal(&queue)
	store.Set(GetQueueIDBytes(queue.Id), b)
}

// GetQueue returns a queue from its id
func (k Keeper) GetQueue(ctx sdk.Context, id uint64) (val types.Queue, found bool) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.QueueKey))
	b := store.Get(GetQueueIDBytes(id))
	if b == nil {
		return val, false
	}
	k.cdc.MustUnmarshal(b, &val)
	return val, true
}

// RemoveQueue removes a queue from the store
func (k Keeper) RemoveQueue(ctx sdk.Context, id uint64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.QueueKey))
	store.Delete(GetQueueIDBytes(id))
}

// GetAllQueue returns all queue
func (k Keeper) GetAllQueue(ctx sdk.Context) (list []types.Queue) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.QueueKey))
	iterator := sdk.KVStorePrefixIterator(store, []byte{})

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var val types.Queue
		k.cdc.MustUnmarshal(iterator.Value(), &val)
		list = append(list, val)
	}

	return
}

// GetQueueIDBytes returns the byte representation of the ID
func GetQueueIDBytes(id uint64) []byte {
	bz := make([]byte, 8)
	binary.BigEndian.PutUint64(bz, id)
	return bz
}

// GetQueueIDFromBytes returns ID in uint64 format from a byte array
func GetQueueIDFromBytes(bz []byte) uint64 {
	return binary.BigEndian.Uint64(bz)
}
