package keeper_test

import (
	"context"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	keepertest "gitlab.com/marcus.appelros/wlqtest/testutil/keeper"
	"gitlab.com/marcus.appelros/wlqtest/x/wlq/keeper"
	"gitlab.com/marcus.appelros/wlqtest/x/wlq/types"
)

func setupMsgServer(t testing.TB) (types.MsgServer, context.Context) {
	k, ctx := keepertest.WlqKeeper(t)
	return keeper.NewMsgServerImpl(*k), sdk.WrapSDKContext(ctx)
}
