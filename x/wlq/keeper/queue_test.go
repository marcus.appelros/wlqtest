package keeper_test

import (
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/require"
	keepertest "gitlab.com/marcus.appelros/wlqtest/testutil/keeper"
	"gitlab.com/marcus.appelros/wlqtest/testutil/nullify"
	"gitlab.com/marcus.appelros/wlqtest/x/wlq/keeper"
	"gitlab.com/marcus.appelros/wlqtest/x/wlq/types"
)

func createNQueue(keeper *keeper.Keeper, ctx sdk.Context, n int) []types.Queue {
	items := make([]types.Queue, n)
	for i := range items {
		items[i].Id = keeper.AppendQueue(ctx, items[i])
	}
	return items
}

func TestQueueGet(t *testing.T) {
	keeper, ctx := keepertest.WlqKeeper(t)
	items := createNQueue(keeper, ctx, 10)
	for _, item := range items {
		got, found := keeper.GetQueue(ctx, item.Id)
		require.True(t, found)
		require.Equal(t,
			nullify.Fill(&item),
			nullify.Fill(&got),
		)
	}
}

func TestQueueRemove(t *testing.T) {
	keeper, ctx := keepertest.WlqKeeper(t)
	items := createNQueue(keeper, ctx, 10)
	for _, item := range items {
		keeper.RemoveQueue(ctx, item.Id)
		_, found := keeper.GetQueue(ctx, item.Id)
		require.False(t, found)
	}
}

func TestQueueGetAll(t *testing.T) {
	keeper, ctx := keepertest.WlqKeeper(t)
	items := createNQueue(keeper, ctx, 10)
	require.ElementsMatch(t,
		nullify.Fill(items),
		nullify.Fill(keeper.GetAllQueue(ctx)),
	)
}

func TestQueueCount(t *testing.T) {
	keeper, ctx := keepertest.WlqKeeper(t)
	items := createNQueue(keeper, ctx, 10)
	count := uint64(len(items))
	require.Equal(t, count, keeper.GetQueueCount(ctx))
}
