package wlq_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	keepertest "gitlab.com/marcus.appelros/wlqtest/testutil/keeper"
	"gitlab.com/marcus.appelros/wlqtest/testutil/nullify"
	"gitlab.com/marcus.appelros/wlqtest/x/wlq"
	"gitlab.com/marcus.appelros/wlqtest/x/wlq/types"
)

func TestGenesis(t *testing.T) {
	genesisState := types.GenesisState{
		Params: types.DefaultParams(),

		QueueList: []types.Queue{
			{
				Id: 0,
			},
			{
				Id: 1,
			},
		},
		QueueCount: 2,
		GameList: []types.Game{
			{
				Id: 0,
			},
			{
				Id: 1,
			},
		},
		GameCount: 2,
		// this line is used by starport scaffolding # genesis/test/state
	}

	k, ctx := keepertest.WlqKeeper(t)
	wlq.InitGenesis(ctx, *k, genesisState)
	got := wlq.ExportGenesis(ctx, *k)
	require.NotNil(t, got)

	nullify.Fill(&genesisState)
	nullify.Fill(got)

	require.ElementsMatch(t, genesisState.QueueList, got.QueueList)
	require.Equal(t, genesisState.QueueCount, got.QueueCount)
	require.ElementsMatch(t, genesisState.GameList, got.GameList)
	require.Equal(t, genesisState.GameCount, got.GameCount)
	// this line is used by starport scaffolding # genesis/test/assert
}
