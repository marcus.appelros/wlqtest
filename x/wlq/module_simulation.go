package wlq

import (
	"math/rand"

	"github.com/cosmos/cosmos-sdk/baseapp"
	simappparams "github.com/cosmos/cosmos-sdk/simapp/params"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/module"
	simtypes "github.com/cosmos/cosmos-sdk/types/simulation"
	"github.com/cosmos/cosmos-sdk/x/simulation"
	"gitlab.com/marcus.appelros/wlqtest/testutil/sample"
	wlqsimulation "gitlab.com/marcus.appelros/wlqtest/x/wlq/simulation"
	"gitlab.com/marcus.appelros/wlqtest/x/wlq/types"
)

// avoid unused import issue
var (
	_ = sample.AccAddress
	_ = wlqsimulation.FindAccount
	_ = simappparams.StakePerAccount
	_ = simulation.MsgEntryKind
	_ = baseapp.Paramspace
)

const (
	opWeightMsgJoinQueue = "op_weight_msg_create_chain"
	// TODO: Determine the simulation weight value
	defaultWeightMsgJoinQueue int = 100

	opWeightMsgSubmitMove = "op_weight_msg_create_chain"
	// TODO: Determine the simulation weight value
	defaultWeightMsgSubmitMove int = 100

	opWeightMsgLeaveQueue = "op_weight_msg_create_chain"
	// TODO: Determine the simulation weight value
	defaultWeightMsgLeaveQueue int = 100

	// this line is used by starport scaffolding # simapp/module/const
)

// GenerateGenesisState creates a randomized GenState of the module
func (AppModule) GenerateGenesisState(simState *module.SimulationState) {
	accs := make([]string, len(simState.Accounts))
	for i, acc := range simState.Accounts {
		accs[i] = acc.Address.String()
	}
	wlqGenesis := types.GenesisState{
		// this line is used by starport scaffolding # simapp/module/genesisState
	}
	simState.GenState[types.ModuleName] = simState.Cdc.MustMarshalJSON(&wlqGenesis)
}

// ProposalContents doesn't return any content functions for governance proposals
func (AppModule) ProposalContents(_ module.SimulationState) []simtypes.WeightedProposalContent {
	return nil
}

// RandomizedParams creates randomized  param changes for the simulator
func (am AppModule) RandomizedParams(_ *rand.Rand) []simtypes.ParamChange {

	return []simtypes.ParamChange{}
}

// RegisterStoreDecoder registers a decoder
func (am AppModule) RegisterStoreDecoder(_ sdk.StoreDecoderRegistry) {}

// WeightedOperations returns the all the gov module operations with their respective weights.
func (am AppModule) WeightedOperations(simState module.SimulationState) []simtypes.WeightedOperation {
	operations := make([]simtypes.WeightedOperation, 0)

	var weightMsgJoinQueue int
	simState.AppParams.GetOrGenerate(simState.Cdc, opWeightMsgJoinQueue, &weightMsgJoinQueue, nil,
		func(_ *rand.Rand) {
			weightMsgJoinQueue = defaultWeightMsgJoinQueue
		},
	)
	operations = append(operations, simulation.NewWeightedOperation(
		weightMsgJoinQueue,
		wlqsimulation.SimulateMsgJoinQueue(am.accountKeeper, am.bankKeeper, am.keeper),
	))

	var weightMsgSubmitMove int
	simState.AppParams.GetOrGenerate(simState.Cdc, opWeightMsgSubmitMove, &weightMsgSubmitMove, nil,
		func(_ *rand.Rand) {
			weightMsgSubmitMove = defaultWeightMsgSubmitMove
		},
	)
	operations = append(operations, simulation.NewWeightedOperation(
		weightMsgSubmitMove,
		wlqsimulation.SimulateMsgSubmitMove(am.accountKeeper, am.bankKeeper, am.keeper),
	))

	var weightMsgLeaveQueue int
	simState.AppParams.GetOrGenerate(simState.Cdc, opWeightMsgLeaveQueue, &weightMsgLeaveQueue, nil,
		func(_ *rand.Rand) {
			weightMsgLeaveQueue = defaultWeightMsgLeaveQueue
		},
	)
	operations = append(operations, simulation.NewWeightedOperation(
		weightMsgLeaveQueue,
		wlqsimulation.SimulateMsgLeaveQueue(am.accountKeeper, am.bankKeeper, am.keeper),
	))

	// this line is used by starport scaffolding # simapp/module/operation

	return operations
}
