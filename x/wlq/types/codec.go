package types

import (
	"github.com/cosmos/cosmos-sdk/codec"
	cdctypes "github.com/cosmos/cosmos-sdk/codec/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/msgservice"
)

func RegisterCodec(cdc *codec.LegacyAmino) {
	cdc.RegisterConcrete(&MsgJoinQueue{}, "wlq/JoinQueue", nil)
	cdc.RegisterConcrete(&MsgSubmitMove{}, "wlq/SubmitMove", nil)
	cdc.RegisterConcrete(&MsgLeaveQueue{}, "wlq/LeaveQueue", nil)
	// this line is used by starport scaffolding # 2
}

func RegisterInterfaces(registry cdctypes.InterfaceRegistry) {
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgJoinQueue{},
	)
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgSubmitMove{},
	)
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgLeaveQueue{},
	)
	// this line is used by starport scaffolding # 3

	msgservice.RegisterMsgServiceDesc(registry, &_Msg_serviceDesc)
}

var (
	Amino     = codec.NewLegacyAmino()
	ModuleCdc = codec.NewProtoCodec(cdctypes.NewInterfaceRegistry())
)
