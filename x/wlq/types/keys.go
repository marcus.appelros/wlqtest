package types

import (
	"time"
)

const (
	// ModuleName defines the module name
	ModuleName = "wlq"

	// StoreKey defines the primary module store key
	StoreKey = ModuleName

	// RouterKey is the message route for slashing
	RouterKey = ModuleName

	// QuerierRoute defines the module's query routing key
	QuerierRoute = ModuleName

	// MemStoreKey defines the in-memory store key
	MemStoreKey = "mem_wlq"
)

func KeyPrefix(p string) []byte {
	return []byte(p)
}

const (
	QueueKey      = "Queue-value-"
	QueueCountKey = "Queue-count-"
)

const (
	GameKey      = "Game-value-"
	GameCountKey = "Game-count-"
)

//Maybe these should go elsewhere?
const (
	MaxTier         = 9
	MaxSizeId       = 2
	MaxTurnDuration = time.Duration(1 * 3_600 * 1000_000_000)
	TimeLayout      = "2006-01-02 15:04:05.999999999 +0000 UTC"
)

var (
	Sizes = [3]uint{3, 5, 9}
)
