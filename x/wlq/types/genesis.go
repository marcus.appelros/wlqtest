package types

import (
	"fmt"
)

// DefaultIndex is the default capability global index
const DefaultIndex uint64 = 1

// DefaultGenesis returns the default Capability genesis state
func DefaultGenesis() *GenesisState {
	q := []Queue{}
	for i := 0; i < MaxTier+1; i++ {
		q = append(q, Queue{uint64(i), []string{}, []string{}, []string{}})
	}
	return &GenesisState{
		QueueList:  q,
		QueueCount: MaxTier + 1,
		GameList:   []Game{},
		// this line is used by starport scaffolding # genesis/types/default
		Params: DefaultParams(),
	}
}

// Validate performs basic genesis state validation returning an error upon any
// failure.
func (gs GenesisState) Validate() error {
	// Check for duplicated ID in queue
	queueIdMap := make(map[uint64]bool)
	queueCount := gs.GetQueueCount()
	for _, elem := range gs.QueueList {
		if _, ok := queueIdMap[elem.Id]; ok {
			return fmt.Errorf("duplicated id for queue")
		}
		if elem.Id >= queueCount {
			return fmt.Errorf("queue id should be lower or equal than the last id")
		}
		queueIdMap[elem.Id] = true
	}
	// Check for duplicated ID in game
	gameIdMap := make(map[uint64]bool)
	gameCount := gs.GetGameCount()
	for _, elem := range gs.GameList {
		if _, ok := gameIdMap[elem.Id]; ok {
			return fmt.Errorf("duplicated id for game")
		}
		if elem.Id >= gameCount {
			return fmt.Errorf("game id should be lower or equal than the last id")
		}
		gameIdMap[elem.Id] = true
	}
	// this line is used by starport scaffolding # genesis/types/validate

	return gs.Params.Validate()
}
