package types

// DONTCOVER

import (
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

// x/wlq module sentinel errors
var (
	ErrAlreadyInQueue = sdkerrors.Register(ModuleName, 1100, "Already in queue")
	ErrNotInQueue     = sdkerrors.Register(ModuleName, 1110, "Not in queue")
	ErrInvalidTier    = sdkerrors.Register(ModuleName, 1200, "No such tier")
	ErrInvalidSizeId  = sdkerrors.Register(ModuleName, 1210, "Invalid sizeId")
	ErrGameNotFound   = sdkerrors.Register(ModuleName, 1300, "Game not found")
	ErrGameFinished   = sdkerrors.Register(ModuleName, 1310, "Game finished")
	ErrNotYourTurn    = sdkerrors.Register(ModuleName, 1320, "Not your turn or you are not in the game")
	ErrOccupied       = sdkerrors.Register(ModuleName, 1330, "That location is occupied")
)
