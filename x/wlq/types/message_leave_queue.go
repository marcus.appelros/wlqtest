package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

const TypeMsgLeaveQueue = "leave_queue"

var _ sdk.Msg = &MsgLeaveQueue{}

func NewMsgLeaveQueue(creator string, tier uint64, sizeId uint64) *MsgLeaveQueue {
	return &MsgLeaveQueue{
		Creator: creator,
		Tier:    tier,
		SizeId:  sizeId,
	}
}

func (msg *MsgLeaveQueue) Route() string {
	return RouterKey
}

func (msg *MsgLeaveQueue) Type() string {
	return TypeMsgLeaveQueue
}

func (msg *MsgLeaveQueue) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgLeaveQueue) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgLeaveQueue) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	if msg.Tier > MaxTier {
		return ErrInvalidTier
	}
	if msg.SizeId > MaxSizeId {
		return ErrInvalidSizeId
	}
	return nil
}
