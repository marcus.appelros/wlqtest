package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

const TypeMsgSubmitMove = "submit_move"

var _ sdk.Msg = &MsgSubmitMove{}

func NewMsgSubmitMove(creator string, gameId uint64, x int32, y int32) *MsgSubmitMove {
	return &MsgSubmitMove{
		Creator: creator,
		GameId:  gameId,
		X:       x,
		Y:       y,
	}
}

func (msg *MsgSubmitMove) Route() string {
	return RouterKey
}

func (msg *MsgSubmitMove) Type() string {
	return TypeMsgSubmitMove
}

func (msg *MsgSubmitMove) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgSubmitMove) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgSubmitMove) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
