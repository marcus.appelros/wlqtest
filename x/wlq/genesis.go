package wlq

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	"gitlab.com/marcus.appelros/wlqtest/x/wlq/keeper"
	"gitlab.com/marcus.appelros/wlqtest/x/wlq/types"
)

// InitGenesis initializes the capability module's state from a provided genesis
// state.
func InitGenesis(ctx sdk.Context, k keeper.Keeper, genState types.GenesisState) {
	// Set all the queue
	for _, elem := range genState.QueueList {
		k.SetQueue(ctx, elem)
	}

	// Set queue count
	k.SetQueueCount(ctx, genState.QueueCount)
	// Set all the game
	for _, elem := range genState.GameList {
		k.SetGame(ctx, elem)
	}

	// Set game count
	k.SetGameCount(ctx, genState.GameCount)
	// this line is used by starport scaffolding # genesis/module/init
	k.SetParams(ctx, genState.Params)
}

// ExportGenesis returns the capability module's exported genesis.
func ExportGenesis(ctx sdk.Context, k keeper.Keeper) *types.GenesisState {
	genesis := types.DefaultGenesis()
	genesis.Params = k.GetParams(ctx)

	genesis.QueueList = k.GetAllQueue(ctx)
	genesis.QueueCount = k.GetQueueCount(ctx)
	genesis.GameList = k.GetAllGame(ctx)
	genesis.GameCount = k.GetGameCount(ctx)
	// this line is used by starport scaffolding # genesis/module/export

	return genesis
}
