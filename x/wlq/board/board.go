package board

import (
	"errors"
	"math"
	"strconv"
	"strings"
)

type Loc struct {
	X, Y int
}
type Unit struct {
	Loc Loc
	P   int //player
}
type Board struct {
	Map    map[Loc]int
	Units  []Unit
	Layers int
	Pi     int
	Ps     []int
}

var connections = [6]Loc{{0, 1}, {1, 0}, {1, -1}, {0, -1}, {-1, 0}, {-1, 1}}

func Makeboard(layers int) Board {
	b := Board{make(map[Loc]int), []Unit{}, layers, 0, []int{1, 2, 3}}
	b.Map[Loc{0, 0}] = 0
	ttgrid := []Loc{Loc{0, 0}}
	for i := 0; i < layers; i++ {
		var tgrid []Loc
		for _, l := range ttgrid {
			for _, c := range connections {
				nl := l
				nl.X += c.X
				nl.Y += c.Y
				d := (math.Abs(float64(nl.X)) + math.Abs(float64(nl.Y)) + math.Abs(float64(nl.X+nl.Y))) / 2
				if int(d) == i+1 {
					//check if nl is already present?
					tgrid = append(tgrid, nl)
				}
			}
		}
		for _, loc := range tgrid {
			b.Map[loc] = 0
		}
		ttgrid = tgrid
	}
	return b
}
func (b *Board) Reset() {
	b.Units = []Unit{}
	b.Pi = 0
	for l := range b.Map {
		b.Map[l] = 0
	}
}
func (b *Board) PlaceP(l Loc, p int) error {
	if val, ok := b.Map[l]; ok {
		if val > 0 {
			return errors.New("Occupied.")
		}
	} else {
		return errors.New("Outside map.")
	}
	b.Map[l] = p
	b.Units = append(b.Units, Unit{l, p})
	return nil
}
func (b *Board) Place(x, y int) error {
	if x == 999 || int(math.Abs(float64(x))+math.Abs(float64(y))+math.Abs(float64(x+y))/2) > b.Layers + 1 {
		b.Pass()
		return nil
	}
	err := b.PlaceP(Loc{x, y}, b.Ps[b.Pi])
	if err != nil {
		return err
	}
	b.Pi += 1
	if b.Pi >= len(b.Ps) {
		b.Pi = 0
	}
	return nil
}
func (b *Board) Pass() {
	b.Pi += 1
	if b.Pi >= len(b.Ps) {
		b.Pi = 0
	}
}
func (b *Board) Adjacent(l Loc) []Loc {
	adj := []Loc{}
	for _, con := range connections {
		tl := l
		tl.X += con.X
		tl.Y += con.Y
		if _, ok := b.Map[tl]; ok {
			adj = append(adj, tl)
		}
	}
	return adj
}
func (b *Board) GetArea(l Loc) map[Loc]struct{} {
	var area = make(map[Loc]struct{})
	area[l] = struct{}{}
	p := b.Map[l]
	c := b.Adjacent(l)
	for len(c) > 0 {
		var tc []Loc
		for _, tl := range c {
			if b.Map[tl] == p {
				_, ok := area[tl]
				if !ok {
					area[tl] = struct{}{}
					tadj := b.Adjacent(tl)
					for _, ttloc := range tadj {
						tc = append(tc, ttloc)
					}
				}
			}
		}
		c = tc
	}
	return area
}
func (b *Board) CountBonds() []uint64 {
	bs := make([]uint64, len(b.Ps))
	for _, unit := range b.Units {
		adj := b.Adjacent(unit.Loc)
		for _, ad := range adj {
			if b.Map[ad] > 0 && b.Map[ad] != unit.P {
				bs[unit.P-1] += 1
			}
		}
	}
	return bs
}
func (b *Board) CountArea() []uint64 {
	if len(b.Units) == 0 {
		return []uint64{0, 0, 0}
	}
	as := make([]uint64, len(b.Ps))
	seg := b.Segment()
	for _, g := range seg.Groups {
		as[g.P-1] += uint64(len(g.Locs))
	}
	for _, s := range seg.Spaces {
		adj := b.AdjacentLocs(s.Locs)
		p := 1
		mono := true
		for l, _ := range adj { //why? Why not?
			p = b.Map[l]
			break
		}
		for l, _ := range adj {
			if b.Map[l] != p {
				mono = false
				break
			}
		}
		if mono {
			as[p-1] += uint64(len(s.Locs))
		}
	}
	return as
}

type Group struct {
	Locs           map[Loc]struct{}
	P              int
	AdjacentGroups []Group
	AdjacentSpaces []Space
	Life           int
}
type Space struct {
	Locs           map[Loc]struct{}
	AdjacentGroups []Group
}
type Segmentation struct {
	Groups []*Group
	Spaces []*Space
	B      *Board
}

func (b *Board) AdjacentLocs(locs map[Loc]struct{}) map[Loc]struct{} {
	adj := make(map[Loc]struct{})
	for loc, _ := range locs {
		a := b.Adjacent(loc)
		for _, l := range a {
			_, ok := locs[l]
			_, ok2 := b.Map[l] //uneccesary?
			if !ok && ok2 {
				adj[l] = struct{}{}
			}
		}
	}
	return adj
}
func (b *Board) Segment() Segmentation {
	var seg Segmentation
	seg.B = b
	checked := make(map[Loc]struct{})
	for loc, p := range b.Map {
		_, ok := checked[loc]
		if !ok {
			a := b.GetArea(loc)
			if p == 0 {
				s := Space{a, []Group{}}
				seg.Spaces = append(seg.Spaces, &s)
			} else {
				g := Group{a, p, []Group{}, []Space{}, 0}
				seg.Groups = append(seg.Groups, &g)
			}
			for l, _ := range a {
				checked[l] = struct{}{}
			}
		}
	}
	return seg
}
func (b *Board) PlaceMap(m map[Loc]struct{}, p int) {
	for l, _ := range m {
		b.PlaceP(l, p)
	}
}
func SetLife(seg Segmentation) {
	for _, group := range seg.Groups {
		tb := Makeboard(seg.B.Layers)
		tb.PlaceMap(group.Locs, group.P)
		tseg := tb.Segment()
		if len(tseg.Spaces) > 1 {
			group.Life = 1
		} else {
			group.Life = 0
		}
	}
}
func (b *Board) Score() ([]uint64, []uint64, uint64) {
	seg := b.Segment()
	SetLife(seg)
	tboard := Makeboard(b.Layers)
	for _, g := range seg.Groups {
		if g.Life > 0 {
			tboard.PlaceMap(g.Locs, g.P)
		}
	}
	bs := tboard.CountBonds()
	as := tboard.CountArea()
	ta := uint64(1)
	for _, a := range as {
		ta = ta * a
	}
	tb := uint64(1)
	for _, a := range bs {
		tb = tb * a
	}
	t := ta * tb
	return as, bs, t
}
func (b *Board) Unitsstr() string {
	res := ""
	for _, unit := range b.Units {
		res += strconv.Itoa(unit.Loc.X) + "," + strconv.Itoa(unit.Loc.Y) + ":" + strconv.Itoa(unit.P) + ";"
	}
	return res
}
func LoadBoard(size int, units string) (*Board, error) {
	board := Makeboard(size)
	places := strings.Split(units, ";")
	var err error
	for _, place := range places {
		if place != "" {
			ta := strings.Split(place, ":")
			locs := strings.Split(ta[0], ",")
			x, err := strconv.Atoi(locs[0])
			if err != nil {
				err = errors.New("Unable to parse some numbers")
				continue
			}
			y, err := strconv.Atoi(locs[1])
			if err != nil {
				err = errors.New("Unable to parse some numbers")
				continue
			}
			p, err := strconv.Atoi(ta[1])
			if err != nil {
				err = errors.New("Unable to parse some numbers")
				continue
			}
			board.PlaceP(Loc{x, y}, p)
		}
	}
	return &board, err
}
func LoadHistory(history string) (*Board, error) {
	arr := strings.Split(history, ";")
	size, err := strconv.Atoi(arr[0])
	if err != nil {
		return nil, errors.New("Unable to parse board size")
	}
	board := Makeboard(size - 1)
	for i := 1; i < len(arr)-1; i++ {
		locs := strings.Split(arr[i], ",")
		if len(locs) != 2 {
			return nil, errors.New("History contains malformed loc")
		}
		x, err := strconv.Atoi(locs[0])
		if err != nil {
			return nil, errors.New("Failed to parse history")
		}
		y, err := strconv.Atoi(locs[1])
		if err != nil {
			return nil, errors.New("Failed to parse history")
		}
		board.Place(x, y)
	}
	return &board, err
}
func ValidateMove(history string, x, y int) uint {
	if x == 999 {
		return 1 //pass
	}
	arr := strings.Split(history, ";")
	size, err := strconv.Atoi(arr[0])
	if err != nil {
		return 3
	}
	d := (math.Abs(float64(x)) + math.Abs(float64(y)) + math.Abs(float64(x+y))) / 2
	if int(d) > size-1 {
		return 1 //pass
	}
	s := strconv.Itoa(x) + "," + strconv.Itoa(y)
	for i := 1; i < len(arr)-1; i++ {
		if s == arr[i] {
			return 2 //occupied
		}
	}
	return 0 //no problems
}
