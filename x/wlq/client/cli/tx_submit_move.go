package cli

import (
	"strconv"

	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/client/flags"
	"github.com/cosmos/cosmos-sdk/client/tx"
	"github.com/spf13/cast"
	"github.com/spf13/cobra"
	"gitlab.com/marcus.appelros/wlqtest/x/wlq/types"
)

var _ = strconv.Itoa(0)

func CmdSubmitMove() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "submit-move [game-id] [x] [y]",
		Short: "Broadcast message submit-move",
		Args:  cobra.ExactArgs(3),
		RunE: func(cmd *cobra.Command, args []string) (err error) {
			argGameId, err := cast.ToUint64E(args[0])
			if err != nil {
				return err
			}
			argX, err := cast.ToInt32E(args[1])
			if err != nil {
				return err
			}
			argY, err := cast.ToInt32E(args[2])
			if err != nil {
				return err
			}

			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			msg := types.NewMsgSubmitMove(
				clientCtx.GetFromAddress().String(),
				argGameId,
				argX,
				argY,
			)
			if err := msg.ValidateBasic(); err != nil {
				return err
			}
			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}
