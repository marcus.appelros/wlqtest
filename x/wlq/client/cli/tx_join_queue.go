package cli

import (
	"strconv"

	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/client/flags"
	"github.com/cosmos/cosmos-sdk/client/tx"
	"github.com/spf13/cast"
	"github.com/spf13/cobra"
	"gitlab.com/marcus.appelros/wlqtest/x/wlq/types"
)

var _ = strconv.Itoa(0)

func CmdJoinQueue() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "join-queue [tier] [size-id]",
		Short: "Broadcast message join-queue",
		Args:  cobra.ExactArgs(2),
		RunE: func(cmd *cobra.Command, args []string) (err error) {
			argTier, err := cast.ToUint64E(args[0])
			if err != nil {
				return err
			}
			argSizeId, err := cast.ToUint64E(args[1])
			if err != nil {
				return err
			}

			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			msg := types.NewMsgJoinQueue(
				clientCtx.GetFromAddress().String(),
				argTier,
				argSizeId,
			)
			if err := msg.ValidateBasic(); err != nil {
				return err
			}
			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}
