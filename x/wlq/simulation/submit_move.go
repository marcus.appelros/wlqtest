package simulation

import (
	"math/rand"

	"github.com/cosmos/cosmos-sdk/baseapp"
	sdk "github.com/cosmos/cosmos-sdk/types"
	simtypes "github.com/cosmos/cosmos-sdk/types/simulation"
	"gitlab.com/marcus.appelros/wlqtest/x/wlq/keeper"
	"gitlab.com/marcus.appelros/wlqtest/x/wlq/types"
)

func SimulateMsgSubmitMove(
	ak types.AccountKeeper,
	bk types.BankKeeper,
	k keeper.Keeper,
) simtypes.Operation {
	return func(r *rand.Rand, app *baseapp.BaseApp, ctx sdk.Context, accs []simtypes.Account, chainID string,
	) (simtypes.OperationMsg, []simtypes.FutureOperation, error) {
		simAccount, _ := simtypes.RandomAcc(r, accs)
		msg := &types.MsgSubmitMove{
			Creator: simAccount.Address.String(),
		}

		// TODO: Handling the SubmitMove simulation

		return simtypes.NoOpMsg(types.ModuleName, msg.Type(), "SubmitMove simulation not implemented"), nil, nil
	}
}
