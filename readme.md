# Weilianqi testnet

To learn more about this project visit its website: https://www.weilianqi.org/

It is currently possible to play through the command line, use `tx wlq join-queue tier sizeId` and `tx wlq submit-move -- gameId x y` while inspecting the state with `q wlq show-game gameId`

Obtain x and y by hovering over locations in [the play section](https://www.weilianqi.org/play.html) of the above website, here you can also load the history string from `show-game`

Below is a series of commands to play a minimal game with non-zero score:

```
wlqtestd tx wlq join-queue 0 0 --from alice -y
wlqtestd tx wlq join-queue 0 0 --from bob -y
wlqtestd tx wlq join-queue 0 0 --from clarice -y
wlqtestd tx wlq submit-move --from clarice -y -- 0 -1 1
wlqtestd tx wlq submit-move --from bob -y -- 0 0 -1
wlqtestd tx wlq submit-move --from alice -y -- 0 1 0
wlqtestd tx wlq submit-move --from clarice -y -- 0 -1 2
wlqtestd tx wlq submit-move --from bob -y -- 0 -1 -1
wlqtestd tx wlq submit-move --from alice -y -- 0 2 -1
wlqtestd tx wlq submit-move --from clarice -y -- 0 -2 1
wlqtestd tx wlq submit-move --from bob -y -- 0 1 -2
wlqtestd tx wlq submit-move --from alice -y -- 0 1 1
wlqtestd tx wlq submit-move --from clarice -y -- 0 0 0
wlqtestd tx wlq submit-move --from bob -y -- 0 999 0
wlqtestd tx wlq submit-move --from alice -y -- 0 999 0
wlqtestd tx wlq submit-move --from clarice -y -- 0 999 0
wlqtestd q wlq show-game 0
```
